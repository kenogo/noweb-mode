# noweb-mode

This is the minor mode I use to do literate programming in noweb.  The idea is
that I can hit `C-c '` to edit the source code block below the pointer in the
desired major mode (`noweb-src-major-mode`), like it is possible to do in org
mode's source code blocks.

When done editing the code block, I hit `C-c '` again and the content is written
into my noweb file.

**This code is buggy**.  For some reason, sometimes only part of the source code
block is written into the editing buffer.  I don't know the reason for this.  If
someone has an answer, I would greatly appreciate hearing about it.  Just send
an email to keno@goertz-berlin.com.
