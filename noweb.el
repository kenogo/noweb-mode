(setq noweb-src-major-mode 'c-mode)

(define-minor-mode noweb-mode
  "Toggle noweb mode for literate programming."
  :init-value nil
  :lighter " Noweb"
  :keymap `((,(kbd "C-c '") . noweb-edit-src-block)))

(defun noweb-src-mode-switch (noweb-point noweb-windows)
  (define-minor-mode noweb-src-mode
    "Minor mode for editing noweb src blocks."
    :init-value nil
    :lighter " NowebSrc"
    :keymap (list (cons (kbd "C-c '")
			(eval `(lambda ()
				 (interactive)
				 (noweb-finish-edit-src-block
				  ,noweb-point ,noweb-windows))))))
  (funcall noweb-src-major-mode)
  (noweb-src-mode))

(defun noweb-get-src-block-start ()
  (if (re-search-backward "^<<.*>>=$" nil t)
      (progn (next-line)
	     (point))
    nil))

(defun noweb-get-src-block-end ()
  (if (re-search-forward "^@" nil t)
      (progn (backward-char)
	     (point))
    nil))

(defun noweb-kill-src-block-text ()
  (let ((end (noweb-get-src-block-end))
	(start (noweb-get-src-block-start)))
    (if (and start end)
	(let ((text (buffer-substring-no-properties
		     start
		     end)))
	  (delete-region start end)
	  text)
      (user-error "No src block found at or after pointer"))))

(defun noweb-edit-src-block ()
  (interactive)
  (let ((text (noweb-kill-src-block-text))
	(noweb-point (point))
	(noweb-windows (current-window-configuration)))
    (pop-to-buffer
     (get-buffer-create "*noweb-src*"))
    (insert text)
    (noweb-src-mode-switch noweb-point noweb-windows)))

(defun noweb-finish-edit-src-block (noweb-point noweb-windows)
  (let ((text (buffer-substring-no-properties (point-min) (point-max))))
    (kill-this-buffer)
    (set-window-configuration noweb-windows)
    ;;(goto-char noweb-point)
    (insert (if (string= (substring text -1 nil) "\n")
		text
	      (concat text "\n")))))
